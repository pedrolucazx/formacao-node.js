import { updateQuantity } from "./item.js";

export async function addItem(cart, newItem) {
  const existingItemIndex = cart.findIndex(
    (item) => item.name === newItem.name
  );

  if (existingItemIndex !== -1) {
    const updatedItem = await updateQuantity(
      cart[existingItemIndex],
      cart[existingItemIndex].quantity + newItem.quantity
    );

    return cart.map((item, index) =>
      index === existingItemIndex ? updatedItem : item
    );
  } else {
    return [...cart, newItem];
  }
}

export async function calcTotal(cart) {
  console.log("Shopee Cart TOTAL IS:");
  const result = cart?.reduce((total, item) => total + item.subtotal, 0);
  console.log(`🛒 Total: ${result}`);
}

export async function deleteItem(cart, id) {
  return cart.filter((item) => item.id !== id);
}

export async function displayCart(cart) {
  console.log("Shopee cart list:");
  cart.forEach((item) => {
    console.log(
      `${item.id}: ${item.name} - ${item.price} | ${item.quantity} | Subtotal = ${item.subtotal}`
    );
  });
}

export async function removeItem(cart, id) {
  const existingItemIndex = cart.findIndex((item) => item.id === id);

  if (existingItemIndex === -1) {
    console.log("Item não encontrado!");
    return cart;
  }

  const existingItem = cart[existingItemIndex];
  const newQuantity = existingItem.quantity - 1;

  if (newQuantity <= 0) {
    return cart.filter((_, index) => index !== existingItemIndex);
  }

  const updatedItem = await updateQuantity(existingItem, newQuantity);
  return cart.map((item, index) =>
    index === existingItemIndex ? updatedItem : item
  );
}
