export async function createItem(id, name, price, quantity) {
  return {
    id,
    name,
    price,
    quantity,
    subtotal: price * quantity,
  };
}

export async function updateQuantity(item, newQuantity) {
  return {
    ...item,
    quantity: newQuantity,
    subtotal: item.price * newQuantity,
  };
}
