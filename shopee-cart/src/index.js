import {
  addItem,
  calcTotal,
  deleteItem,
  displayCart,
  removeItem,
} from "./services/cart.js";
import { createItem } from "./services/item.js";

let myCart = [];
let nextId = 1;

console.log("Welcome to the your Shopee Cart!");

const item1 = await createItem(nextId++, "Produto A", 10.0, 2);
const item2 = await createItem(nextId++, "Produto A", 10.0, 4);
const item3 = await createItem(nextId++, "Produto B", 20.0, 2);
const item4 = await createItem(nextId++, "Produto C", 20.0, 2);

myCart = await addItem(myCart, item1);
myCart = await addItem(myCart, item2);
myCart = await addItem(myCart, item3);
myCart = await addItem(myCart, item4);

await displayCart(myCart);

myCart = await removeItem(myCart, 3);
myCart = await removeItem(myCart, 1);
myCart = await deleteItem(myCart, 4);

myCart = await displayCart(myCart);
await calcTotal(myCart);
