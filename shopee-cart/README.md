# Carrinho de Compras da Shopee com Node.js

# Sobre

Este projeto implementa a lógica por trás de um carrinho de compras, similar ao da Shopee. O carrinho armazena itens, calcula automaticamente o total e os subtotais dos itens, proporcionando uma base sólida para a organização e modularização de projetos.

# **Objetivos e Resultados**

- **Modularização de Projetos**: Aprender a dividir um projeto em módulos de forma eficiente.
- **Organização do Pensamento Lógico e Funcional**: Desenvolver a habilidade de estruturar o pensamento para resolver problemas de maneira lógica e funcional.
- **Base para Organização de Projetos**: Estabelecer uma base para organizar e estruturar futuros projetos de software.

# Funcionalidades

- Adicionar itens ao carrinho.
- Remover itens do carrinho.
- Calcular automaticamente o subtotal de cada item.
- Calcular o total do carrinho.
- Atualizar quantidades de itens no carrinho.

# Tecnologias Utilizadas

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)

# Como Utilizar

```bash
git clone https://gitlab.com/pedrolucazx/formacao-node.js.git

cd formacao-node.js/shopee-cart

node src/index.js

```

[Referência](https://github.com/digitalinnovationone/formacao-nodejs/tree/main/06-shopee-cart)
