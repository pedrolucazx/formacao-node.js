# Podcast Manager

# Sobre

Podcast Manager é uma aplicação que centraliza diferentes episódios de podcasts, organizando-os por categorias para facilitar a navegação e descoberta de novos conteúdos.

# **Objetivos e Resultados**

- **Estruturação de Projetos:** Organização eficaz do projeto em diferentes camadas.
- **Implementação sem Frameworks:** Desenvolvimento da API sem depender de frameworks externos, seguindo as boas práticas como o Princípio da Responsabilidade Única (SRP).
- **Conceitos de API REST/RESTful:** Aplicação dos fundamentos essenciais para criar uma arquitetura robusta e altamente integrável.

# Funcionalidades

- **Filtragem:** Permite filtrar episódios por nome, categoria ou ambos simultaneamente.
- **Listagem de Podcasts:** Retorna uma lista de todos os podcasts disponíveis.
- **Detalhes do Podcast por ID:** Retorna os detalhes de um podcast específico com base no ID fornecido.
- **Criação de Novo Podcast:** Cria um novo podcast com os dados fornecidos no corpo da requisição.
- **Atualização de Podcast Existente:** Atualiza os dados de um podcast existente com base no ID fornecido e nos dados fornecidos no corpo da requisição.
- **Exclusão de Podcast:** Remove um podcast existente com base no ID fornecido.
- **Filtragem de Podcasts:** Permite filtrar os podcasts com base no episódio, nome do podcast e/ou categoria. Retorna os podcasts que correspondem aos critérios especificados nos parâmetros de consulta.

# Endpoints

```
GET /podcasts
GET /podcast/{id}
POST /podcast
PUT /podcast/{id}
DELETE /podcast/{id}
GET /podcasts/filter?episode={episode}&podcast={podcast}&category={category}
```

# Tecnologias Utilizadas

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)

# Como Utilizar

```bash
git clone https://gitlab.com/pedrolucazx/formacao-node.js.git

cd formacao-node.js/podcast-manager

yarn start:dev || npm run start:dev

```

[Referência](https://github.com/felipeAguiarCode/node-ts-webapi-without-frameworks-podcast-menager)
