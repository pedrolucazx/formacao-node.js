import { ServerResponse } from "http";
import { STATUSCODE } from "./enums/status-codes";

export const successHandle = (
  response: ServerResponse,
  statuscode: STATUSCODE,
  data: any
): void => {
  response.writeHead(statuscode, { "Content-Type": "application/json" });
  response.end(JSON.stringify(data));
};
