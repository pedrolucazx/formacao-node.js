import { ServerResponse } from "http";
import { STATUSCODE } from "./enums/status-codes";

export const errorHandle = (
  response: ServerResponse,
  statuscode: STATUSCODE,
  message: string
): void => {
  response.writeHead(statuscode, { "Content-Type": "application/json" });
  response.end(JSON.stringify({ error: message }));
};
