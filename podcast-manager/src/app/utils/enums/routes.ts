export enum ROUTES {
  PODCAST = "/api/podcast",
  LIST = "/api/podcast/list",
  FILTER = "/api/podcast/filter",
}
