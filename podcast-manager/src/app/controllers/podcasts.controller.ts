import { IncomingMessage, ServerResponse } from "node:http";
import podcastService from "../services/podcast.service";
import { STATUSCODE } from "../utils/enums/status-codes";
import { errorHandle } from "../utils/errorHandler";
import { parseRequestBody } from "../utils/parseRequestBody";
import { successHandle } from "../utils/successHandle";

const getAllPodcasts = async (
  _request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const podcasts = await podcastService.getAllPodcasts();
    successHandle(response, STATUSCODE.OK, podcasts);
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Serve Error"
    );
  }
};

const getPodcastById = async (
  request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const id = request.url!.split("/")[3];
    const podcast = await podcastService.getPodcastsById(id);
    podcast
      ? successHandle(response, STATUSCODE.OK, podcast)
      : errorHandle(response, STATUSCODE.NOT_FOUND, "Podcast not found");
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Serve Error"
    );
  }
};

const createNewPodcast = async (
  request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const body = await parseRequestBody(request);
    const newPodcast = await podcastService.createPodcast(body);
    successHandle(response, STATUSCODE.CREATED, newPodcast);
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Serve Error"
    );
  }
};

const updatePodcast = async (
  request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const id = request.url!.split("/")[3];
    const body = await parseRequestBody(request);
    const updatedPodcast = await podcastService.updatePodcast(id, body);
    updatedPodcast
      ? successHandle(response, STATUSCODE.OK, updatedPodcast)
      : errorHandle(response, STATUSCODE.NOT_FOUND, "Podcast not found");
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Server Error"
    );
  }
};

const deletePodcast = async (
  request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const id = request.url!.split("/")[3];
    const success = await podcastService.deletePodcast(id);
    if (success) {
      response.writeHead(STATUSCODE.NO_CONTENT);
      response.end();
    } else {
      errorHandle(response, STATUSCODE.NOT_FOUND, "Podcast not found");
    }
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Server Error"
    );
  }
};

const filterPodcasts = async (
  request: IncomingMessage,
  response: ServerResponse
) => {
  try {
    const filter = Object.fromEntries(
      new URLSearchParams(request.url!.split("?")[1]).entries()
    );

    const filteredPodcasts = await podcastService.filterPodcasts(filter);
    successHandle(response, STATUSCODE.OK, filteredPodcasts);
  } catch (error) {
    errorHandle(
      response,
      STATUSCODE.INTERNAL_SERVER_ERROR,
      "Internal Server Error"
    );
  }
};

export default {
  getAllPodcasts,
  getPodcastById,
  createNewPodcast,
  updatePodcast,
  deletePodcast,
  filterPodcasts,
};
