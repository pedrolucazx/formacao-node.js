import { IncomingMessage, ServerResponse } from "node:http";
import podcastsController from "../controllers/podcasts.controller";
import { HTTPMETHOD } from "../utils/enums/http-methods";
import { ROUTES } from "../utils/enums/routes";
import { STATUSCODE } from "../utils/enums/status-codes";
import { errorHandle } from "../utils/errorHandler";

const podcastRoutes = (request: IncomingMessage, response: ServerResponse) => {
  const path = request.url;
  const method = request.method?.toUpperCase();

  if (path?.startsWith(ROUTES.FILTER) && method === HTTPMETHOD.GET) {
    podcastsController.filterPodcasts(request, response);
  } else if (path === ROUTES.LIST && method === HTTPMETHOD.GET) {
    podcastsController.getAllPodcasts(request, response);
  } else if (path === ROUTES.PODCAST && method === HTTPMETHOD.POST) {
    podcastsController.createNewPodcast(request, response);
  } else if (path?.startsWith(ROUTES.PODCAST) && method === HTTPMETHOD.GET) {
    podcastsController.getPodcastById(request, response);
  } else if (path?.startsWith(ROUTES.PODCAST) && method === HTTPMETHOD.PUT) {
    podcastsController.updatePodcast(request, response);
  } else if (path?.startsWith(ROUTES.PODCAST) && method === HTTPMETHOD.DELETE) {
    podcastsController.deletePodcast(request, response);
  } else {
    errorHandle(response, STATUSCODE.INTERNAL_SERVER_ERROR, "Route not found");
  }
};
export default podcastRoutes;
