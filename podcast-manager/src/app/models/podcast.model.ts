export interface Podcast {
  id: string;
  podcast: string;
  episode: string;
  categories: string[];
}

export interface Filters {
  episode?: string;
  podcast?: string;
  category?: string;
}
