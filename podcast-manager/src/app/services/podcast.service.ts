import { Filters, Podcast } from "../models/podcast.model";
import podcastRepository from "../repositories/podcast.repository";

const getAllPodcasts = async (): Promise<Podcast[]> => {
  return await podcastRepository.getAllPodcasts();
};

const getPodcastsById = async (id: string): Promise<Podcast | undefined> => {
  return await podcastRepository.getPodcastById(id);
};

const createPodcast = async (
  podcast: Omit<Podcast, "id">
): Promise<Podcast> => {
  return await podcastRepository.createPodcast(podcast);
};

const updatePodcast = async (
  id: string,
  podcast: Partial<Podcast>
): Promise<Podcast | null> => {
  return await podcastRepository.updatePodcast(id, podcast);
};

const deletePodcast = async (id: string): Promise<boolean> => {
  return await podcastRepository.deletePodcast(id);
};

const filterPodcasts = async (filters: Filters): Promise<Podcast[]> => {
  const allPodcasts = await podcastRepository.getAllPodcasts();
  return allPodcasts.filter((podcast) => {
    const filterEpisode =
      filters.episode &&
      podcast.episode.toLowerCase().includes(filters.episode.toLowerCase());
    const filterPodcast =
      filters.podcast &&
      podcast.podcast.toLowerCase().includes(filters.podcast.toLowerCase());
    const filterCategory =
      filters.category &&
      podcast.categories.some!(
        (category) => category.toLowerCase() === filters.category!.toLowerCase()
      );

    return filterEpisode || filterPodcast || filterCategory;
  });
};

export default {
  getAllPodcasts,
  getPodcastsById,
  createPodcast,
  updatePodcast,
  deletePodcast,
  filterPodcasts,
};
