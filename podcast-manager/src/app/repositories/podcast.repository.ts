import { randomUUID } from "node:crypto";
import fs from "node:fs/promises";
import { join } from "node:path";
import { Podcast } from "../models/podcast.model";

const filePath = join(__dirname, "../data/podcasts.json");

const readFile = async (): Promise<Podcast[]> => {
  const data = await fs.readFile(filePath, "utf-8");
  return JSON.parse(data) as Podcast[];
};

const writeFile = async (data: Podcast[]): Promise<void> => {
  await fs.writeFile(filePath, JSON.stringify(data, null, 2), "utf-8");
};

const getAllPodcasts = async (): Promise<Podcast[]> => {
  return await readFile();
};

const getPodcastById = async (id: string): Promise<Podcast | undefined> => {
  const podcasts = await readFile();
  return podcasts.find((podcast) => podcast.id === id);
};

const createPodcast = async (
  podcast: Omit<Podcast, "id">
): Promise<Podcast> => {
  const podcasts = await readFile();
  const newPodcast = { ...podcast, id: randomUUID() };
  podcasts.push(newPodcast);
  await writeFile(podcasts);
  return newPodcast;
};

const updatePodcast = async (
  id: string,
  updatedFields: Partial<Podcast>
): Promise<Podcast | null> => {
  const podcasts = await readFile();
  const index = podcasts.findIndex((podcast) => podcast.id === id);
  if (index != -1) {
    podcasts[index] = { ...podcasts[index], ...updatedFields };
    await writeFile(podcasts);
    return podcasts[index];
  }

  return null;
};

const deletePodcast = async (id: string): Promise<boolean> => {
  const podcasts = await readFile();
  const index = podcasts.findIndex((podcast) => podcast.id === id);
  if (index != -1) {
    podcasts.splice(index, 1);
    await writeFile(podcasts);
    return true;
  }
  return false;
};

export default {
  getAllPodcasts,
  getPodcastById,
  createPodcast,
  updatePodcast,
  deletePodcast,
};
