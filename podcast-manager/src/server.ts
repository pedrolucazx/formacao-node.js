import http from "node:http";
import routers from "./app/routes";

const PORT = process.env.PORT;
const server = http.createServer(routers);

server.listen(PORT, () => console.log(`Server is running port: ${PORT} 🚀`));
