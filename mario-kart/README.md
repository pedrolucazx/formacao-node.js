# Desafio de Projeto: Mario Kart.JS

<table>
  <tr>
    <td>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/header.gif" alt="Mario Kart" width="200">
    </td>
    <td>
      <h1>Objetivo:</h1>
      <p>Mario Kart é uma série de jogos de corrida desenvolvida e publicada pela Nintendo. Nosso desafio será criar uma lógica de um jogo de vídeo game para simular corridas de Mario Kart, levando em consideração as regras e mecânicas abaixo.</p>
    </td>
  </tr>
</table>

## Players

<table style="border-collapse: collapse; width: 800px; margin: 0 auto;">
  <tr>
    <td style="border: 1px solid black; text-align: center;">
      <p>Mario</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/mario.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 4</p>
      <p>Manobrabilidade: 3</p>
      <p>Poder: 3</p>
    </td>
    <td style="border: 1px solid black; text-align: center;">
      <p>Peach</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/peach.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 3</p>
      <p>Manobrabilidade: 4</p>
      <p>Poder: 2</p>
    </td>
    <td style="border: 1px solid black; text-align: center;">
      <p>Yoshi</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/yoshi.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 2</p>
      <p>Manobrabilidade: 4</p>
      <p>Poder: 3</p>
    </td>
  </tr>
  <tr>
    <td style="border: 1px solid black; text-align: center;">
      <p>Bowser</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/bowser.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 5</p>
      <p>Manobrabilidade: 2</p>
      <p>Poder: 5</p>
    </td>
    <td style="border: 1px solid black; text-align: center;">
      <p>Luigi</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/luigi.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 3</p>
      <p>Manobrabilidade: 4</p>
      <p>Poder: 4</p>
    </td>
    <td style="border: 1px solid black; text-align: center;">
      <p>Donkey Kong</p>
      <img src="https://raw.githubusercontent.com/digitalinnovationone/formacao-nodejs/main/03-projeto-mario-kart/docs/dk.gif" alt="Mario Kart" width="60" height="60">
      <p>Velocidade: 2</p>
      <p>Manobrabilidade: 2</p>
      <p>Poder: 5</p>
    </td>
  </tr>
</table>

### 🕹️ Regras & Mecânicas

**Jogadores:**

- O Computador deve receber dois personagens para disputar a corrida em um objeto cada

**Pistas:**

- Os personagens irão correr em uma pista aleatória de 5 rodadas
- A cada rodada, será sorteado um bloco da pista que pode ser uma reta, curva ou confronto
  - Caso o bloco da pista seja uma RETA, o jogador deve jogar um dado de 6 lados e somar o atributo VELOCIDADE, quem vencer ganha um ponto
  - Caso o bloco da pista seja uma CURVA, o jogador deve jogar um dado de 6 lados e somar o atributo MANOBRABILIDADE, quem vencer ganha um ponto
  - Caso o bloco da pista seja um CONFRONTO, o jogador deve jogar um dado de 6 lados e somar o atributo PODER, quem perder, perde um ponto
  - Nenhum jogador pode ter pontuação negativa (valores abaixo de 0)

**Confronto:**

- Sortear aleatoriamente se é um casco(-1 ponto) ou uma bomba(-2 pontos)
- Quem vence o confronto ganha um turbo (+1 ponto) aleatoriamente

**Condição de Vitória:**

- Ao final, vence quem acumulou mais pontos

# **Objetivos e Resultados**

- **Aplicar Princípios SRP (Princípio da Responsabilidade Única)**: Criar funções específicas para cada ação esperada, garantindo que cada função tenha uma única responsabilidade bem definida.
- **Organização do Pensamento Lógico e Funcional**: Desenvolver a habilidade de estruturar o pensamento para resolver problemas de maneira lógica e funcional.

# Tecnologias Utilizadas

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)

# Como Utilizar

```bash
git clone https://gitlab.com/pedrolucazx/formacao-node.js.git

cd formacao-node.js/mario-kart

node src/index.js

```

[Referência](https://github.com/digitalinnovationone/formacao-nodejs/tree/main/03-projeto-mario-kart)
