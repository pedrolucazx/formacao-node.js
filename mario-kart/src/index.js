const { PLAYERS, BLOCK } = require("./constants");

const generateRandomPlayers = (players) => {
  const playerKeys = Object.keys(players);
  const randomIndex1 = Math.floor(Math.random() * playerKeys.length);
  let randomIndex2;
  do {
    randomIndex2 = Math.floor(Math.random() * playerKeys.length);
  } while (randomIndex2 === randomIndex1);
  const player1 = players[playerKeys[randomIndex1]];
  const player2 = players[playerKeys[randomIndex2]];
  return [player1, player2];
};

const rollDice = async () => Math.floor(Math.random() * 6 + 1);

const getRandomBlock = async () => {
  const randomBlock = Math.floor(Math.random() * BLOCK.length);
  return BLOCK[randomBlock];
};

const logRollResult = (characterName, block, diceResult, attribute) => {
  console.log(
    `${characterName} 🎲 Resultado do dado é ${diceResult}. Novo valor do(a) ${block} -> ${diceResult} + ${attribute} = ${
      diceResult + attribute
    }`
  );
};

const rollDiceAndCalculateTotalSkill = async (character, block) => {
  const diceResult = await rollDice();
  let totalSkill = 0;

  if (block === "RETA") {
    totalSkill = diceResult + character.speed;
    logRollResult(character.name, "Velocidade", diceResult, totalSkill);
  }

  if (block === "CURVA") {
    totalSkill = diceResult + character.maneuverability;
    logRollResult(character.name, "Manobrabilidade", diceResult, totalSkill);
  }

  if (block === "CONFRONTO") {
    totalSkill = diceResult + character.power;
  }

  return totalSkill;
};

const resolveConfront = async (
  character1,
  character2,
  diceResult1,
  diceResult2
) => {
  let powerResult1 = diceResult1 + character1.power;
  let powerResult2 = diceResult2 + character2.power;

  console.log(`${character1.name} confrontou com ${character2.name}! 🥊`);

  if (powerResult1 > powerResult2 && character2.points > 0) {
    console.log(
      `${character1.name} venceu o confronto! ${character2.name} perdeu 1 ponto 🐢`
    );
    character2.points--;
  } else if (powerResult2 > powerResult1 && character1.points > 0) {
    console.log(
      `${character2.name} venceu o confronto! ${character1.name} perdeu 1 ponto 🐢`
    );
    character1.points--;
  } else {
    console.log("Confronto empatado! Nenhum ponto foi perdido");
  }
};

const verifyWinner = async (
  totalSkillPlayer1,
  totalSkillPlayer2,
  character1,
  character2
) => {
  return totalSkillPlayer1 > totalSkillPlayer2
    ? (console.log(`${character1.name} marcou 1️⃣ ponto! `), character1.points++)
    : (console.log(`${character2.name} marcou 1️⃣ ponto!`), character2.points++);
};

const resolveRound = async (
  block,
  character1,
  character2,
  totalSkillPlayer1,
  totalSkillPlayer2
) => {
  return block === "CONFRONTO"
    ? await resolveConfront(
        character1,
        character2,
        totalSkillPlayer1,
        totalSkillPlayer2
      )
    : await verifyWinner(
        totalSkillPlayer1,
        totalSkillPlayer2,
        character1,
        character2
      );
};

const playRaceEngine = async (character1, character2) => {
  for (let round = 1; round <= 5; round++) {
    console.log(`🏁 Rodada ${round}`);

    const block = await getRandomBlock();
    console.log(`MODO DE JOGO SERÁ: ${block}`);

    const totalSkillPlayer1 = await rollDiceAndCalculateTotalSkill(
      character1,
      block
    );

    const totalSkillPlayer2 = await rollDiceAndCalculateTotalSkill(
      character2,
      block
    );

    await resolveRound(
      block,
      character1,
      character2,
      totalSkillPlayer1,
      totalSkillPlayer2
    );
    console.log("-----------------------------");
  }
};

const declareWinner = async (character1, character2) => {
  console.log(
    `Resultado final -> ${character1.name} = ${character1.points} Ponto(s) vs ${character2.name} = ${character2.points}`
  );

  const vencedor =
    character1.points > character2.points
      ? character1.name
      : character2.points > character1.points
      ? character2.name
      : "empate";

  console.log(
    vencedor === "empate"
      ? "A corrida terminou em empate"
      : `\n${vencedor} venceu a corrida! Parabéns! 🏆`
  );
};

(async function main() {
  const [player1, player2] = generateRandomPlayers(PLAYERS);
  console.log(
    `🏁🚨 Corrida entre ${player1.name} e ${player2.name} começando...\n`
  );
  await playRaceEngine(player1, player2);
  await declareWinner(player1, player2);
})();
