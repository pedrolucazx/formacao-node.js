const PLAYERS = {
  player1: {
    name: "Mario",
    speed: 4,
    maneuverability: 3,
    power: 3,
    points: 0,
    totalSkill: 0,
  },
  player2: {
    name: "Peach",
    speed: 3,
    maneuverability: 4,
    power: 2,
    points: 0,
    totalSkill: 0,
  },
  player3: {
    name: "Yoshi",
    speed: 2,
    maneuverability: 4,
    power: 3,
    points: 0,
    totalSkill: 0,
  },
  player4: {
    name: "Bowser",
    speed: 5,
    maneuverability: 2,
    power: 5,
    points: 0,
    totalSkill: 0,
  },
  player5: {
    name: "Luigi",
    speed: 3,
    maneuverability: 4,
    power: 4,
    points: 0,
    totalSkill: 0,
  },
  player6: {
    name: "Donkey Kong",
    speed: 2,
    maneuverability: 2,
    power: 5,
    points: 0,
    totalSkill: 0,
  },
};

const BLOCK = ["RETA", "CURVA", "CONFRONTO"];

module.exports = { PLAYERS, BLOCK };
