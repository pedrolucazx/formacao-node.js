# Formação Node.js Fundamentals

# Sobre

Este repositório tem como objetivo armazenar todos os Desafios de Projetos do curso Formação Node.js Fundamentals da DIO.

🚗 [Simulador de Corridas do Mario Kart com Node.js](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/mario-kart)

🛒 [Carrinho de Compras da Shopee com Node.js](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/shopee-cart)

🔑 [Gerador de QRcode para e-commerce](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/qrcode)

🎙️ [Gerenciador de Podcast - API NodeJS Com Typescript e HTTP Module](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/podcast-manager)

🏎️ [Criando uma Minimal API da Fórmula 1 com Node.js e Fastify](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/formula-one)

⚽ [Recriando a API da Champions League com Node.js e Express](https://gitlab.com/pedrolucazx/formacao-node.js/-/tree/main/champions-league)

# Porque **Node.js** como carreira?

Assim como os coalas, que se adaptam habilmente às diferentes árvores e desafios da vida selvagem, aprender Node.js é abraçar a flexibilidade e eficiência no desenvolvimento Web

# Timeline

**2009** - Node.js foi criado por Ryan Dahl e lançado como um ambiente de execução de Javascript do lado do servidor

**2010** - A versão 0.1.14 do Node.js foi lançada, marcando o início do desenvolvimento da comunidade e o crescente interesse na plataforma

**2011** - O Node.js foi adotado por grandes empresas como Microsoft, Yahoo! e LinkedIn, aumentando ainda mais sua popularidade.

**2012** - A formação da Fundação Node.js como uma organização sem fins lucrativos para gerenciar e evoluir o projeto. Mais tarde como OpenJS Foundation.

**2013** - Lançamento da versão 0.10 do Node.js, trazendo melhorias de desempenho e estabilidade.

**2015** - A versão 0.12 introduziu o suporte oficial para o ECMAScript 6 (ES6), trazendo recursos modernos ao JavaScript

**2016** - O lançamento da versão 6 do Node.js, marcado a transição para um ciclo de lançamento de versões Long Term Support (LTS)

**2020** - Ryan Dahl cria o Deno, o maior concorrente do Node.js, dessa vez fazendo tudo do jeito que ele queria.

# O que o Node não é?

✅ Node.js não é uma linguagem de programação, a **linguagem é Javascript.**

✅ Não é uma framework é uma **runtime (ambiente de execução) de Javascript.**

✅ Node não é limitado, ele faz tudo que outras **tecnologia de backend fazem.**

# Tipos de módulos

### **CommonJS**

O CommonJS utiliza uma sintaxe mais simples com as palavras `require` para carregar módulos e `module.exports` para exporta funcionalidades

### **ESM**

Os módulos ESM introduzem uma sintaxe unificada com as palavras `import` e `export` alinhada com padrões de outras linguagens modernas.
