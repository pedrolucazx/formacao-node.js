# Formula One

## Sobre

Formula One é uma aplicação que centraliza diferentes times e motoristas de Fórmula 1.

## Objetivos e Resultados

- **Estruturação de Projetos:** Organização eficaz do projeto em diferentes camadas para melhor manutenção e escalabilidade.
- **Implementação com Fastify:** Desenvolvimento da API usando Fastify, com plugins como o Swagger para documentação da aplicação.
- **Conceitos de API REST/RESTful:** Aplicação dos fundamentos essenciais para criar uma arquitetura robusta e altamente integrável.

## Funcionalidades

- **Listagem de Times:** Retorna uma lista de todos os times disponíveis.
- **Listagem de Motoristas:** Retorna uma lista de todos os motoristas disponíveis.
- **Buscar Time por ID:** Retorna os detalhes de um time específico com base no ID fornecido.
- **Buscar Motorista por ID:** Retorna os detalhes de um motorista específico com base no ID fornecido.

## Tecnologias Utilizadas

![Fastify](https://img.shields.io/badge/fastify-%23000000.svg?style=for-the-badge&logo=fastify&logoColor=white)
![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![Swagger](https://img.shields.io/badge/-Swagger-%23Clojure?style=for-the-badge&logo=swagger&logoColor=white)

## Como Utilizar

1. Clone o repositório:

   ```bash
   git clone https://gitlab.com/pedrolucazx/formula-one
   ```

2. Navegue até o diretório do projeto:

   ```bash
   cd formacao-node.js/formula-one
   ```

3. Inicie a aplicação:

   ```bash
   yarn start:dev
   # ou
   npm run start:dev
   ```

4. Acesse a documentação da API no Swagger:

   ```
   http://localhost:3000/api/swagger/
   ```

## Referência

[Formação Node.js - Digital Innovation One](https://github.com/digitalinnovationone/formacao-nodejs/tree/main/project-formula-1/node-f1)
