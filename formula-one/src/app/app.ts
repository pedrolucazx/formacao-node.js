import cors from "@fastify/cors";
import fastifySwagger from "@fastify/swagger";
import fastifySwaggerUi from "@fastify/swagger-ui";
import fastify, { FastifyInstance } from "fastify";
import { corsOptions, swaggerOptions, swaggerUiOptions } from "../app/config";
import { driverRouter, teamRouter } from "./routes";

const app: FastifyInstance = fastify({ logger: true });

app.register(cors, corsOptions);

app.register(fastifySwagger, swaggerOptions);
app.register(fastifySwaggerUi, swaggerUiOptions);
app.register(teamRouter, { prefix: "/api/teams" });
app.register(driverRouter, { prefix: "/api/drivers" });

app.addHook("onRoute", (routeOptions) => {
  if (routeOptions.path.startsWith("/api/teams")) {
    routeOptions.schema = routeOptions.schema || {};
    routeOptions.schema.tags = ["Teams"];
  } else if (routeOptions.path.startsWith("/api/drivers")) {
    routeOptions.schema = routeOptions.schema || {};
    routeOptions.schema.tags = ["Drivers"];
  }
});

export default app;
