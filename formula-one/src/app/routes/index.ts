import { FastifyInstance } from "fastify";
import driverController from "../controllers/driver.controller";
import teamController from "../controllers/team.controller";

export const teamRouter = async (route: FastifyInstance) => {
  route.get("/", teamController.getAllTeams);
  route.get("/:id", teamController.getTeamById);
};

export const driverRouter = async (route: FastifyInstance) => {
  route.get("/", driverController.getAllDrivers);
  route.get("/:id", driverController.getDriverById);
};
