const PORT = Number(process.env.PORT) || 8080;

export const swaggerOptions = {
  swagger: {
    info: {
      title: "My Title",
      description: "My Description.",
      version: "1.0.0",
    },
    host: `localhost:${PORT}`,
    schemes: ["http", "https"],
    consumes: ["application/json"],
    produces: ["application/json"],
    tags: [
      { name: "Teams", description: "Teams related endpoints" },
      { name: "Drivers", description: "Drivers related endpoints" },
    ],
  },
};

export const swaggerUiOptions = {
  routePrefix: "/api/swagger",
  exposeRoute: true,
};
