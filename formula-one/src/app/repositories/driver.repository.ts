import fs from "node:fs/promises";
import { join } from "node:path";
import { Driver } from "../models/driver.model";

const filePath = join(__dirname, "../data/drivers.json");

const readFile = async (): Promise<Driver[]> => {
  const data = await fs.readFile(filePath, "utf-8");
  return JSON.parse(data) as Driver[];
};

const writeFile = async (data: Driver[]): Promise<void> => {
  await fs.writeFile(filePath, JSON.stringify(data, null, 2), "utf-8");
};

const getAllDrivers = async (): Promise<Driver[]> => await readFile();

const getDriverById = async (id: number): Promise<Driver | undefined> => {
  const drivers = await readFile();
  return drivers.find((driver) => driver.id === id);
};

export default {
  getAllDrivers,
  getDriverById,
};
