import fs from "node:fs/promises";
import { join } from "node:path";
import { Team } from "../models/team.model";

const filePath = join(__dirname, "../data/teams.json");

const readFile = async (): Promise<Team[]> => {
  const data = await fs.readFile(filePath, "utf-8");
  return JSON.parse(data) as Team[];
};

const writeFile = async (data: Team[]): Promise<void> => {
  await fs.writeFile(filePath, JSON.stringify(data, null, 2), "utf-8");
};

const getAllTeams = async (): Promise<Team[]> => await readFile();

const getTeamById = async (id: number): Promise<Team | undefined> => {
  const teams = await readFile();
  return teams.find((team) => team.id === id);
};

export default {
  getAllTeams,
  getTeamById,
};
