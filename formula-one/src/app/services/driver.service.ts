import { Driver } from "../models/driver.model";
import driverRepository from "../repositories/driver.repository";

const getAllDrivers = async (): Promise<Driver[]> => {
  return driverRepository.getAllDrivers();
};

const getDriverById = async (id: number): Promise<Driver | undefined> => {
  return driverRepository.getDriverById(id);
};

export default { getAllDrivers, getDriverById };
