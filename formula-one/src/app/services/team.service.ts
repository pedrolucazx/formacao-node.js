import { Team } from "../models/team.model";
import teamRepository from "../repositories/team.repository";

const getAllTeams = async (): Promise<Team[]> => {
  return teamRepository.getAllTeams();
};

const getTeamById = async (id: number): Promise<Team | undefined> => {
  return teamRepository.getTeamById(id);
};

export default { getAllTeams, getTeamById };
