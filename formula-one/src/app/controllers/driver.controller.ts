import { FastifyReply, FastifyRequest } from "fastify";
import driverService from "../services/driver.service";

const getAllDrivers = async (_request: FastifyRequest, reply: FastifyReply) => {
  const drivers = await driverService.getAllDrivers();
  reply.send(drivers);
};

const getDriverById = async (
  request: FastifyRequest<{ Params: { id: number } }>,
  reply: FastifyReply
) => {
  const driver = await driverService.getDriverById(Number(request.params.id));
  driver
    ? reply.send(driver)
    : reply.status(400).send({ message: "Driver not found" });
};

export default { getAllDrivers, getDriverById };
