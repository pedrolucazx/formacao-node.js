import { FastifyReply, FastifyRequest } from "fastify";
import teamService from "../services/team.service";

const getAllTeams = async (_request: FastifyRequest, reply: FastifyReply) => {
  const teams = await teamService.getAllTeams();
  reply.send(teams);
};

const getTeamById = async (
  request: FastifyRequest<{ Params: { id: number } }>,
  reply: FastifyReply
) => {
  const team = await teamService.getTeamById(Number(request.params.id));
  team
    ? reply.send(team)
    : reply.status(400).send({ message: "Team not found" });
};

export default { getAllTeams, getTeamById };
