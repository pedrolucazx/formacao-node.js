# Champions League API

## Sobre

Este repositório contém a implementação do backend de uma API da Champions League, desenvolvida como parte do projeto de formação em Node.js pela Digital Innovation One. A API fornece endpoints para gerenciar jogadores e times dentro do contexto de um campeonato de futebol.

### Objetivos e Conquistas

- **Estruturação do Projeto:** Organização eficaz do projeto em diferentes camadas para melhor manutenção e escalabilidade.
- **Implementação com Express:** Desenvolvimento da API utilizando o framework Express.
- **Conceitos de API REST/RESTful:** Aplicação dos fundamentos essenciais para criar uma arquitetura robusta e altamente integrável.

## Funcionalidades

- **Listagem de Jogadores:** Retorna uma lista de todos os jogadores disponíveis.
- **Detalhes do Jogador por ID:** Retorna os detalhes de um jogador específico com base no ID fornecido.
- **Criação de Novo Jogador:** Cria um novo jogador com os dados fornecidos no corpo da requisição.
- **Atualização de Jogador Existente:** Atualiza os dados de um jogador existente com base no ID fornecido e nos dados fornecidos no corpo da requisição.
- **Exclusão de Jogador:** Remove um jogador existente com base no ID fornecido.
- **Listagem de Times:** Retorna uma lista de todos os times disponíveis.

## Endpoints

```
GET /players",
GET /players/{id}
POST /players",
PUT /players/{id}
DELETE /players/{id}
GET /clubs",
```

# Tecnologias Utilizadas

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![TypeScript](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![Express.js](https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB)

## Como Utilizar

1. Clone o repositório:

   ```bash
   git clone https://gitlab.com/pedrolucazx/formula-one
   ```

2. Navegue até o diretório do projeto:

   ```bash
   cd formacao-node.js/formula-one
   ```

3. Inicie a aplicação:

   ```bash
   yarn start:dev
   # ou
   npm run start:dev
   ```

## Referência

[Formação Node.js - Digital Innovation One](https://github.com/digitalinnovationone/formacao-nodejs/tree/main/project-formula-1/node-f1)
