import app from "./app/app";

const PORT = Number(process.env.PORT) || 8080;

const server = () => {
  try {
    app.listen({ port: PORT });
    console.log(`Server listening on port ${PORT}`);
  } catch (error) {
    console.error("Error starting server:", error);
    process.exit(1);
  }
};

server();
