import { Request, Response } from "express";
import {
  errorResponse,
  notFoundResponse,
  successResponse,
} from "../helpers/http.helper";
import clubsService from "../services/clubs.service";

export const getClubs = async (_request: Request, response: Response) => {
  try {
    const data = await clubsService.getClubs();
    if (data) {
      return successResponse(response, data);
    } else {
      return notFoundResponse(response);
    }
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

export default { getClubs };
