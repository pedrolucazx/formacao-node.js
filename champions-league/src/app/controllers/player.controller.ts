import { Request, Response } from "express";
import {
  errorResponse,
  notFoundResponse,
  successResponse,
} from "../helpers/http.helper";
import playersService from "../services/players.service";

const getAllPlayer = async (_request: Request, response: Response) => {
  try {
    const data = await playersService.getPlayers();
    if (data) {
      return successResponse(response, data);
    } else {
      return notFoundResponse(response);
    }
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

const getPlayerById = async (request: Request, response: Response) => {
  try {
    const id = parseInt(request.params.id);
    const player = await playersService.getPlayerById(id);
    if (player) {
      return successResponse(response, player);
    } else {
      return notFoundResponse(response, "Player not found");
    }
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

const createPlayer = async (request: Request, response: Response) => {
  try {
    const newPlayer = { ...request.body, id: 0 };
    const player = await playersService.addPlayer(newPlayer);
    return successResponse(response, player, 201);
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

const updatePlayer = async (request: Request, response: Response) => {
  try {
    const playerId = parseInt(request.params.id, 10);
    const updatedPlayer = { ...request.body, id: playerId };
    const player = await playersService.editPlayer(playerId, updatedPlayer);
    if (player) {
      return successResponse(response, player);
    } else {
      return notFoundResponse(response, "Player not found");
    }
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

const deletePlayer = async (request: Request, response: Response) => {
  try {
    const playerId = parseInt(request.params.id, 10);
    const data = await playersService.removePlayer(playerId);
    return successResponse(response, data, 204);
  } catch (error) {
    console.error("Error retrieving user:", error);
    return errorResponse(response, "Internal Server Error");
  }
};

export default {
  getAllPlayer,
  getPlayer: getPlayerById,
  createPlayer,
  updatePlayer,
  deletePlayer,
};
