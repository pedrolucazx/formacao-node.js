import { Player } from "../models/Player.model";
import playerRepository from "../repositories/player.repository";

const getPlayers = async (): Promise<Player[]> => {
  try {
    return await playerRepository.getAllPlayers();
  } catch (error) {
    throw error;
  }
};

const getPlayerById = async (id: number): Promise<Player | undefined> => {
  try {
    return await playerRepository.getPlayerById(id);
  } catch (error) {
    throw error;
  }
};

const addPlayer = async (player: Player): Promise<Player> => {
  try {
    return await playerRepository.createPlayer(player);
  } catch (error) {
    throw error;
  }
};

const editPlayer = async (
  id: number,
  player: Player
): Promise<Player | null> => {
  try {
    return await playerRepository.updatePlayer(id, player);
  } catch (error) {
    throw error;
  }
};

const removePlayer = async (id: number): Promise<void> => {
  try {
    return await playerRepository.deletePlayer(id);
  } catch (error) {
    throw error;
  }
};

export default {
  getPlayers,
  getPlayerById,
  addPlayer,
  editPlayer,
  removePlayer,
};
