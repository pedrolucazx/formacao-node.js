import { Club } from "../models/Club.model";
import clubRepository from "../repositories/club.repository";

const getClubs = async (): Promise<Club[]> => {
  try {
    return await clubRepository.getAllClubs();
  } catch (error) {
    throw error;
  }
};

export default { getClubs };
