import { Router } from "express";
import clubController from "../controllers/club.controller";
import playerController from "../controllers/player.controller";

const router = Router();

router.get("/players", playerController.getAllPlayer);
router.get("/players/:id", playerController.getPlayer);
router.post("/players", playerController.createPlayer);
router.put("/players/:id", playerController.updatePlayer);
router.delete("/players/:id", playerController.deletePlayer);
router.get("/clubs", clubController.getClubs);

export default router;
