import { Response } from "express";

export const successResponse = (
  response: Response,
  data: any,
  status = 200
) => {
  return response.status(status).json(data);
};

export const errorResponse = (
  response: Response,
  message: string,
  status = 500
) => {
  return response.status(status).json({ error: true, message });
};

export const notFoundResponse = (response: Response, message = "Not Found") => {
  return errorResponse(response, message, 404);
};
