import fs from "fs/promises";
import { join } from "path";
import { Club } from "../models/Club.model";

const filePath = join(__dirname, "../data/clubs.json");

const readFile = async (): Promise<Club[]> => {
  const data = await fs.readFile(filePath, "utf-8");
  return JSON.parse(data) as Club[];
};

const getAllClubs = async (): Promise<Club[]> => {
  return await readFile();
};

export default { getAllClubs };
