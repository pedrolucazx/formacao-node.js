import { Player } from "../models/Player.model";

let players: Player[] = [
  {
    id: 1,
    name: "Lionel Messi",
    club: "Paris Saint-Germain",
    nationality: "Argentina",
    position: "Forward",
    statistics: {
      overall: 93,
      pace: 85,
      shooting: 94,
      passing: 91,
      dribbling: 95,
      defending: 38,
      physical: 65,
    },
  },
  {
    id: 2,
    name: "Cristiano Ronaldo",
    club: "Manchester United",
    nationality: "Portugal",
    position: "Forward",
    statistics: {
      overall: 92,
      pace: 89,
      shooting: 93,
      passing: 82,
      dribbling: 88,
      defending: 35,
      physical: 78,
    },
  },
];
let nextId = 1;

const getAllPlayers = async (): Promise<Player[]> => {
  return players;
};

const getPlayerById = async (id: number): Promise<Player | undefined> => {
  console.log(id);
  return players.find((player) => player.id === id);
};

const createPlayer = async (player: Player): Promise<Player> => {
  player.id = nextId++;
  players.push(player);
  return player;
};

const updatePlayer = async (
  id: number,
  updatedPlayer: Player
): Promise<Player | null> => {
  const index = players.findIndex((player) => player.id === id);
  if (index !== -1) {
    players[index] = updatedPlayer;
    return updatedPlayer;
  }
  return null;
};

const deletePlayer = async (id: number): Promise<void> => {
  players = players.filter((player) => player.id !== id);
};

export default {
  getAllPlayers,
  getPlayerById,
  createPlayer,
  updatePlayer,
  deletePlayer,
};
