import prompt from "prompt";

export async function getUserInput(schema) {
  prompt.start();
  return prompt.get(schema);
}
