import chalk from "chalk";
import qr from "qrcode-terminal";
import { promptQRCode } from "../../prompts-schema/prompt-qrcode.js";
import { getUserInput } from "../../utils/prompt-utils.js";

async function generateQRCode(link, type) {
  return new Promise((resolve) => {
    qr.generate(link, { small: type == 2 }, (qrcode) => {
      resolve(qrcode);
    });
  });
}

export async function createQRCode() {
  try {
    const result = await getUserInput(promptQRCode);
    const qrcode = await generateQRCode(result.link, result.type);
    console.log(chalk.green.bold("✅ QR Code gerado com sucesso:\n"));
    console.log(qrcode);
  } catch (error) {
    console.error(chalk.red("Error on application"), error);
  }
}
