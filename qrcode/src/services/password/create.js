import chalk from "chalk";

function generatePasswordCharacters() {
  let characters = [];

  if (process.env.UPPERCASE_LETTERS === "true") {
    characters.push(..."ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  }

  if (process.env.LOWERCASE_LETTERS === "true") {
    characters.push(..."abcdefghijklmnopqrstuvwxyz");
  }

  if (process.env.NUMBERS === "true") {
    characters.push(..."0123456789");
  }

  if (process.env.SPECIAL_CHARACTERS === "true") {
    characters.push(..."!@#$%^*()_-=+");
  }

  return characters;
}

function getRandomCharacter(characters) {
  const index = Math.floor(Math.random() * characters.length);
  return characters[index];
}

export async function createPassword() {
  try {
    console.log(chalk.green("Password"));

    const passwordLength = parseInt(process.env.PASSWORD_LENGTH, 10);
    const characters = generatePasswordCharacters();
    let password = "";

    for (let i = 0; i < passwordLength; i++) {
      password += getRandomCharacter(characters);
    }

    console.log(password);
  } catch (error) {
    console.error(chalk.red("Error generating password"), error);
  }
}
