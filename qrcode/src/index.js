import chalk from "chalk";
import { mainPrompt } from "./prompts-schema/prompt-main.js";
import { createPassword } from "./services/password/create.js";
import { createQRCode } from "./services/qr-code/create.js";
import { getUserInput } from "./utils/prompt-utils.js";

async function main() {
  try {
    const { select } = await getUserInput(mainPrompt);
    if (select == 1) {
      await createQRCode();
    } else if (select == 2) {
      await createPassword();
    }
  } catch (error) {
    console.error(chalk.red("Error in main execution"), error);
  }
}

main();
