# Gerador de QRcode para e-commerce

# Sobre

Este é um projeto que visa simplificar a geração de QR Codes e senhas aleatórias para uso em e-commerce.

# **Objetivos e Resultados**

- **Modularização de Projetos**: Divisão eficiente do projeto em módulos.
- **Uso de async/await:** Facilitação da leitura e manipulação de código assíncrono.
- **Tratamento de erros:** Implementação de blocos try/catch para capturar e lidar com erros.
- **Legibilidade:** Organização do código para facilitar manutenção e expansão futura.
- **Configuração centralizada:** Uso de um arquivo .env para configurações sensíveis.

# Funcionalidades

- Geração de QR Codes.
- Geração de senhas aleatórias.

# Tecnologias Utilizadas

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)

# Como Utilizar

```bash
git clone https://gitlab.com/pedrolucazx/formacao-node.js.git

cd formacao-node.js/qrcode

yarn start || npm run start

```

[Referência](https://github.com/digitalinnovationone/formacao-nodejs/tree/main/projeto-qrcode)
